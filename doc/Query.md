# 查询过滤器
查询过滤器Web.Filter.QueryFilterAttribute主要用于给**添加修改、查询删除**操作方法设置条件。   

## 1、查询删除过滤器  
### (1)实现步骤
#### 第1步、给控制器添加[QueryFilter]属性注解
#### 第2步、操作方法命名包含["list", "delete", "batchdelete", "get", "query", "delete"]任何一个（不区分大小写）或者方法添加[Queryable]注解
#### 

